//
//  DMContentCreatorTests.m
//  DMContentCreatorTests
//
//  Created by Trash on 9/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMContentCreatorTests.h"
#import "DMAddressSelector.h"
@implementation DMContentCreatorTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (NSArray *)proList
{
    DMAddressSelector *selector = [DMAddressSelector new];
    NSArray *arr = [selector provinceList];
    return arr;
}

- (void)testExample
{
    NSArray *arr;
    arr = [self proList];
    NSLog(@"test -> %@ ",arr);
}

@end
