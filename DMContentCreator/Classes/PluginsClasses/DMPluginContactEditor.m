//
//  DMPluginContactEditor.m
//  DMContentCreator
//
//  Created by Trash on 11/26/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMPluginContactEditor.h"
#import <iOS7Colors/UIColor+iOS7Colors.h>
#import "DMContentCreator.h"
#import "DMContentCreatorStyle.h"
#import <LAUtilities/LAUtilities.h>
#import <FXBlurView/FXBlurView.h>
#import <WYPopoverController/WYPopoverController.h>
#import "DMAddressSelector.h"
#import "DMPickerViewController.h"

typedef enum {
    DMTimeOpen,
    DMTimeClose
}DMTimeTag;

@interface DMPluginContactEditor ()<UIPickerViewDelegate,UIPickerViewDataSource>{
    NSDictionary *userData;
    WYPopoverController *datePopover,*addressPopover;
    BOOL isShowTimePicker,isShowAddressPicker;
    BOOL animating;
    DMAddressSelector *addressSelector;
    NSMutableArray *tmpList;
    BOOL isDismissing;
//    NSNumber *province,*district,*subdistrict;
    NSUInteger proRow,distRow,subDistRow;
    NSDateFormatter *dateFormatter,*displayFormatter;
}
@property (weak, nonatomic) IBOutlet UISwitch *switchOpen24Hours;
@property (weak, nonatomic) IBOutlet UISwitch *switchSameAddress;
@property (weak, nonatomic) IBOutlet UISwitch *switchSameContact;


@property (weak, nonatomic) IBOutlet UILabel *timeLabelOpen;
@property (weak, nonatomic) IBOutlet UILabel *timeLabelClose;

@property (weak, nonatomic) IBOutlet UITextView *informationTextView;
@property (weak, nonatomic) IBOutlet UITextView *addressTextview;

@property (weak, nonatomic) IBOutlet UILabel *addressProvinceLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressDistrictLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressSubDistrictLabel;
@property (weak, nonatomic) IBOutlet UITextField *addressPostCodeTextField;


@property (weak, nonatomic) IBOutlet UITextField *contactFacebook;
@property (weak, nonatomic) IBOutlet UITextField *contactTwitter;
@property (weak, nonatomic) IBOutlet UITextField *contactGooglePlus;
@property (weak, nonatomic) IBOutlet UITextField *contactSite;
@property (weak, nonatomic) IBOutlet UITextField *contactEmail;
@property (weak, nonatomic) IBOutlet UITextField *contactPhone;
@property (weak, nonatomic) IBOutlet UITextField *contactFax;
@property (weak, nonatomic) IBOutlet UITextView *contactETC;

// left title
@property (weak, nonatomic) IBOutlet UILabel *hourLabel;
@property (weak, nonatomic) IBOutlet UILabel *openLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeLabel;
@property (weak, nonatomic) IBOutlet UILabel *additionalLabel;
@property (weak, nonatomic) IBOutlet UILabel *sameProfileLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *provinceLabel;
@property (weak, nonatomic) IBOutlet UILabel *districtLabel;
@property (weak, nonatomic) IBOutlet UILabel *subDistrictLabel;
@property (weak, nonatomic) IBOutlet UILabel *postCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *sameContactLabel;
@property (weak, nonatomic) IBOutlet UILabel *facebookLabel;
@property (weak, nonatomic) IBOutlet UILabel *twitterLabel;
@property (weak, nonatomic) IBOutlet UILabel *googlePlusLabel;
@property (weak, nonatomic) IBOutlet UILabel *webSiteLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *faxLabel;
@property (weak, nonatomic) IBOutlet UILabel *etcLabel;

@property (nonatomic,weak) DMContentPlugins *plugins;

@property (nonatomic,strong) DMPickerViewController *datePickerVC,*addressPickerVC;
@end

@implementation DMPluginContactEditor

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [DMContentCreatorStyle setNavigationBarStyle:self.navigationController];
    self.navigationItem.leftBarButtonItem = [DMContentCreatorStyle closeButtonWithHandler:^(UIBarButtonItem *weakSender) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    self.title = _plugins.pluginName;
    if ([UIDevice systemLessThan:@"7.0"]) {
        [self.tableView setBackgroundView:nil];
        [self.tableView setBackgroundColor:[[DMContentCreator sharedComponents] themeColor]];
    }
    
    _switchOpen24Hours.onTintColor =
    _switchSameAddress.onTintColor =
    _switchSameContact.onTintColor = [[DMContentCreator sharedComponents] color];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    displayFormatter = [NSDateFormatter new];
    [displayFormatter setDateFormat:@"h:mm a"];
    
    userData = [[DMContentCreator sharedComponents] userData];
    
    NSLog(@"user : %@",userData);
    
    addressSelector = [[DMAddressSelector alloc] init];
    
    self.datePickerVC = [[DMPickerViewController alloc] initWithType:DMPickerTypeDate];
    
    self.addressPickerVC = [[DMPickerViewController alloc] initWithType:DMPickerTypeAddress];
    [self.addressPickerVC setAddressDelegate:self addressDatasource:self];
    
    if (!_plugins[DMContactAllTime]) {
        _plugins[DMContactAllTime] = @NO;
    }
}

#pragma mark - Picker Datasource & Delegate

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [tmpList count];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return tmpList[row][@"name"];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 45.0f;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self displayFromPickerView:pickerView];
}

#pragma mark -

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self display];
    [self switchNeedDisplay:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    [self save];
    [self prepareData];
    NSArray *list = [_plugins checkIncompleteLists];
    NSLog(@"%@",list);
    [super viewDidDisappear:animated];
}


- (IBAction)switch24Hours:(UISwitch *)sender {
    _plugins[DMContactAllTime] = @(sender.on);
    [self setTimeSectionEnable:!sender.on];
}

-(void)setTimeSectionEnable:(BOOL)enable{
    _timeLabelClose.enabled =
    _timeLabelOpen.enabled = enable;
}

- (IBAction)switchSameAddress:(UISwitch *)sender {
    if (sender.on) {
        for (NSString *key in @[DMContactAddress,DMContactProvinceId,DMContactDistrictId,DMContactSubDistrictId,DMContactPostCode]) {
            [self transferUserDataToPluginKey:key];
        }
    }
    _plugins[DMContactSameAddress] = sender.on ? userData[@"userid"]:@0;
    [self setAddressSectionEnable:!sender.on];
    [self display];
}

-(void)transferUserDataToPluginKey:(NSString *)key{
    _plugins[key] = userData[key];
    NSLog(@"plugin[%@] = %@",key,_plugins[key]);
}

-(void)setAddressSectionEnable:(BOOL)enable{
    _addressTextview.editable =
    _addressProvinceLabel.enabled =
    _addressDistrictLabel.enabled =
    _addressSubDistrictLabel.enabled =
    _addressPostCodeTextField.enabled = enable;
}
- (IBAction)print:(id)sender {
    [self save];
    NSLog(@"%@",_plugins.dataSource);
}

- (IBAction)switchSameContact:(UISwitch *)sender {
    if (sender.on){
        for (NSString *key in @[DMContactFacebook,DMContactTwitter,DMContactGooglePlus,DMContactSite,DMContactEmail,DMContactPhone,DMContactFax]) {
            [self transferUserDataToPluginKey:key];
        }
    }
    _plugins[DMContactSameContact] = sender.on ? userData[@"userid"]: @0;
    [self display];
    [self setContactSectionEnable:!sender.on];
}

-(void)display{
    
    //OPENING TIMES
    [_switchOpen24Hours setOn:[_plugins[DMContactAllTime] integerValue] animated:NO];
    if (_plugins[DMContactOpenHours]) {
        [_timeLabelOpen setText:[displayFormatter stringFromDate:[dateFormatter dateFromString:_plugins[DMContactOpenHours]]]];
    }
    if (_plugins[DMContactCloseHours]) {
        [_timeLabelClose setText:[displayFormatter stringFromDate:[dateFormatter dateFromString:_plugins[DMContactCloseHours]]]];
    }
    [_informationTextView setText:_plugins[DMContactInformation]];
    
    [_hourLabel setText:NSLocalizedString(@"24 Hours", nil)];
    [_openLabel setText:NSLocalizedString(@"Open time", nil)];
    [_closeLabel setText:NSLocalizedString(@"Close time", nil)];
    [_additionalLabel setText:NSLocalizedString(@"Additional Information", nil)];
    
    //ADDRESS
    [_switchSameAddress setOn:[_plugins[DMContactSameAddress] integerValue] animated:NO];
    [_addressTextview setText:_plugins[DMContactAddress]];
    [_addressProvinceLabel setText:[addressSelector provinceWithProID:_plugins[DMContactProvinceId]]];
    [_addressDistrictLabel setText:[addressSelector districtWithDisID:_plugins[DMContactDistrictId]]];
    [_addressSubDistrictLabel setText:[addressSelector subDistrictWithSubDistID:_plugins[DMContactSubDistrictId]]];
    [_addressPostCodeTextField setText:_plugins[DMContactPostCode]];
    
    _sameProfileLabel.text = _sameContactLabel.text = NSLocalizedString(@"Same as your profile", nil);
    [_addressLabel setText:NSLocalizedString(@"Address", nil)];
    [_provinceLabel setText:NSLocalizedString(@"Province", nil)];
    [_districtLabel setText:NSLocalizedString(@"District", nil)];
    [_subDistrictLabel setText:NSLocalizedString(@"Sub District", nil)];
    [_postCodeLabel setText:NSLocalizedString(@"Postal Code", nil)];
    
    //CONTACT
    [_switchSameContact setOn:[_plugins[DMContactSameContact] integerValue] animated:NO];
    [_contactFacebook setText:_plugins[DMContactFacebook]];
    [_contactTwitter setText:_plugins[DMContactTwitter]];
    [_contactGooglePlus setText:_plugins[DMContactGooglePlus]];
    [_contactSite setText:_plugins[DMContactSite]];
    [_contactEmail setText:_plugins[DMContactEmail]];
    [_contactPhone setText:_plugins[DMContactPhone]];
    [_contactFax setText:_plugins[DMContactFax]];
    [_contactETC setText:_plugins[DMContactETC]];
    
    [_facebookLabel setText:NSLocalizedString(@"Facebook", nil)];
    [_twitterLabel setText:NSLocalizedString(@"Twitter", nil)];
    [_googlePlusLabel setText:NSLocalizedString(@"Google+", nil)];
    [_webSiteLabel setText:NSLocalizedString(@"Website", nil)];
    [_emailLabel setText:NSLocalizedString(@"Email", nil)];
    [_phoneLabel setText:NSLocalizedString(@"Phone", nil)];
    [_faxLabel setText:NSLocalizedString(@"Fax", nil)];
    [_etcLabel setText:NSLocalizedString(@"ETC", nil)];
    
    [self save];
}

-(void)switchNeedDisplay:(BOOL)need{
    [self switch24Hours:_switchOpen24Hours];
    [self switchSameContact:_switchSameContact];
    [self switchSameAddress:_switchSameAddress];
}

-(void)setContactSectionEnable:(BOOL)enable{
    _contactFacebook.enabled =
    _contactTwitter.enabled =
    _contactGooglePlus.enabled =
    _contactSite.enabled =
    _contactEmail.enabled =
    _contactPhone.enabled =
    _contactFax.enabled =
    _contactETC.editable = enable;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
    switch (indexPath.section) {
        case 0:
            if (!_switchOpen24Hours.on) {
                if (indexPath.row == 1 || indexPath.row ==2) {
                    [self slidePickerIn:indexPath.row atFrame:[tableView cellForRowAtIndexPath:indexPath].frame];
                }
            }
            break;
        case 1:
            if (!_switchSameAddress.on) {
                if (indexPath.row >1 && indexPath.row <5) {
                    switch (indexPath.row) {
                        case 3:
                            if (!_plugins[DMContactProvinceId]) {
                                return;
                            }
                            break;
                        case 4:{
                            if (!DMContactProvinceId||!DMContactDistrictId) {
                                return;
                            }
                        }
                        default:
                            break;
                    }
                     [self slideAddressPickerIn:indexPath.row atFrame:[tableView cellForRowAtIndexPath:indexPath].frame];
                }
            }
        default:
            break;
    }
}

- (void )slidePickerIn:(NSUInteger)section atFrame:(CGRect)frame{
    //Position the picker out of site
    NSDate *initDate = [NSDate new];
//    if (section == 0 && _plugins[DMCBeginDate]) {
//        initDate = [dateFormatter dateFromString:_plugins[DMCBeginDate]];
//    }else if (_plugins[DMCEndDate]) {
//        initDate = [dateFormatter dateFromString:_plugins[DMCEndDate]];
//    }else{
//        initDate = [NSDate date];
//    }
    DMTimeTag tagMode = section == 1 ? DMTimeOpen : DMTimeClose;
    
    NSString *dateString =  _plugins[tagMode == DMTimeOpen ? DMContactOpenHours : DMContactCloseHours];
    if (!dateString) {
        initDate = [NSDate new];
//        initDate = [NSDate dateFromString:@"00:00:00" format:@"HH:mm:ss"];
    }else{
        initDate = [dateFormatter dateFromString:dateString];
    }
    
    self.datePickerVC.datePicker.tag = tagMode;
    [self.datePickerVC.datePicker addTarget:self action   :@selector(dateChange:) forControlEvents:UIControlEventValueChanged];
    
    if (initDate) {
        [self.datePickerVC.datePicker setDate:initDate animated:YES];
    }
    [self dateChange:self.datePickerVC.datePicker];
    //Add the picker to the view
    
    if (!datePopover) {
        datePopover = [[WYPopoverController alloc] initWithContentViewController:self.datePickerVC];
        [datePopover setPopoverContentSize:CGSizeMake(300, 200)];
    }
    [datePopover presentPopoverFromRect:frame
                             inView:self.tableView
           permittedArrowDirections:WYPopoverArrowDirectionAny
                           animated:YES
                            options:WYPopoverAnimationOptionFadeWithScale
                             completion:nil];
    
}

- (void )slideAddressPickerIn:(NSUInteger)section atFrame:(CGRect)frame{
    self.addressPickerVC.addressPicker.tag = section;
    switch (section) {
        case 2:
            tmpList = [addressSelector provinceList];
            break;
        case 3:
            tmpList = [addressSelector districtListWithProID:_plugins[DMContactProvinceId]];
            break;
        case 4:
            tmpList = [addressSelector subdistrictListWithDisID:_plugins[DMContactDistrictId] proID:_plugins[DMContactProvinceId]];
            break;
        default:
            break;
    }
    [self.addressPickerVC.addressPicker reloadAllComponents];
    //Add the picker to the view
    
    if (!addressPopover) {
        addressPopover =  [[WYPopoverController alloc] initWithContentViewController:self.addressPickerVC];
        [addressPopover setPopoverContentSize:CGSizeMake(300, 220)];
    }
    
    [addressPopover presentPopoverFromRect:frame
                                    inView:self.tableView
                  permittedArrowDirections:WYPopoverArrowDirectionAny
                                  animated:YES
                                   options:WYPopoverAnimationOptionFadeWithScale
                                completion:^{
                                    [self.addressPickerVC.addressPicker reloadAllComponents];
                                    [self scrollPickerFromSection:section];
                                }];
}

-(void)scrollPickerFromSection:(NSUInteger)section{
    switch (section) {
        case 2:
            [self.addressPickerVC.addressPicker selectRow:proRow inComponent:0 animated:NO];
            break;
        case 3:
            [self.addressPickerVC.addressPicker selectRow:distRow inComponent:0 animated:NO];
            break;
        case 4:
            [self.addressPickerVC.addressPicker selectRow:subDistRow inComponent:0 animated:NO];
            break;
        default:
            break;
    }
    [self displayFromPickerView:self.addressPickerVC.addressPicker];

}

-(void)displayFromPickerView:(UIPickerView *)pickerView{
    NSInteger row =[self.addressPickerVC.addressPicker selectedRowInComponent:0];
    switch (pickerView.tag) {
        case 2:
            proRow = row;
            _addressProvinceLabel.text = tmpList[row][@"name"];
            _addressDistrictLabel.text = nil;
            _addressSubDistrictLabel.text = nil;
            _plugins[DMContactProvinceId] = tmpList[row][@"id"];
            [_plugins removeObjectForKey:DMContactDistrictId];
            [_plugins removeObjectForKey:DMContactSubDistrictId];
            break;
        case 3:
            distRow = row;
            _addressDistrictLabel.text = tmpList[row][@"name"];
            _addressSubDistrictLabel.text = nil;
            _plugins[DMContactDistrictId] = tmpList[row][@"id"];
            [_plugins removeObjectForKey:DMContactSubDistrictId];
            break;
        case 4:
            subDistRow = row;
            _addressSubDistrictLabel.text = tmpList[row][@"name"];
            _plugins[DMContactSubDistrictId] = tmpList[row][@"id"];;
            break;
    }
}

- (void)prepareData{
    NSLog(@"%@",_plugins.dataSource);
    _plugins[DMContactCountryId] = @210;
    [_plugins checkIncompleteLists];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    if (scrollView.contentOffset.y < -170.0f && !isDismissing) {
        isDismissing = YES;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)dateChange:(UIDatePicker *)__datePicker{
    DMTimeTag mode = __datePicker.tag;
    _plugins[mode == DMTimeOpen ? DMContactOpenHours : DMContactCloseHours] = [dateFormatter stringFromDate:__datePicker.date];
    
    UILabel *targetLabel = mode == DMTimeOpen ? _timeLabelOpen : _timeLabelClose;
    [targetLabel setText:[displayFormatter stringFromDate:__datePicker.date]];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


-(void)save{
    _plugins[DMContactAllTime] = @(_switchOpen24Hours.on);
    _plugins[DMContactInformation] = _informationTextView.text;
    
    _plugins[DMContactAddress] = _addressTextview.text;
    _plugins[DMContactPostCode] = _addressPostCodeTextField.text;
    
    _plugins[DMContactFacebook] = _contactFacebook.text;
    _plugins[DMContactTwitter] = _contactTwitter.text;
    _plugins[DMContactGooglePlus] = _contactGooglePlus.text;
    _plugins[DMContactSite] = _contactSite.text;
    _plugins[DMContactEmail] = _contactEmail.text;
    _plugins[DMContactPhone] = _contactPhone.text;
    _plugins[DMContactFax] = _contactFax.text;
    _plugins[DMContactETC] = _contactETC.text;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @[@"Opening times",@"Address",@"Contact"][section];
    return NSLocalizedString(title, nil);
}

@end
