//
//  DMPluginPollEditor.h
//  DMContentCreator
//
//  Created by Trash on 11/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMContentPlugins.h"
@interface DMPluginPollEditor : UITableViewController<DMContentPluginProtocol>

@end
