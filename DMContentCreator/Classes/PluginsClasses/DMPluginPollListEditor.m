//
//  DMPluginPollListEditor.m
//  DMContentCreator
//
//  Created by Trash on 11/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMPluginPollListEditor.h"
#import <BlocksKit/BlocksKit.h>
#import <BlocksKit/BlocksKit+UIKit.h>
#import <LAUtilities/LAUtilities.h>
#import <iOS7Colors/UIColor+iOS7Colors.h>
#import "DMContentCreator.h"
@interface DMPluginPollListEditor ()

@end

@implementation DMPluginPollListEditor

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.navigationItem.rightBarButtonItem.tintColor  = [[DMContentCreator sharedComponents] foregroundColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_choices count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row >= [_choices count]) {
        [cell.textLabel setText:NSLocalizedString(@"Add new choice", nil)];
        [cell.textLabel setTextColor:[UIColor iOS7lightGrayColor]];
        return cell;
    }
    
    [[cell textLabel] setText:_choices[indexPath.row][@"value"]];
    [[cell textLabel] setTextColor:[[DMContentCreator sharedComponents] color]];
    return cell;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= [_choices count]) {
        return NO;
    }
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [_choices removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}



// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView
moveRowAtIndexPath:(NSIndexPath *)fromIndexPath
      toIndexPath:(NSIndexPath *)toIndexPath
{
    if (toIndexPath.row >= [_choices count]) {
        [tableView reloadData];
        return;
    }
    [_choices insertObject: [_choices objectAtIndex:fromIndexPath.row] atIndex:toIndexPath.row];
    [_choices removeObjectAtIndex:(fromIndexPath.row + 1)];
}



// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row >= [_choices count]) {
        return NO;
    }
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row >= [_choices count]) {
        __block UIAlertView *blockAlert;
        UIAlertView *alertView = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"New choice", nil) message:NSLocalizedString(@"MSG for alert caption point"  , nil)];
        blockAlert = alertView;
        [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alertView bk_setCancelButtonWithTitle:NSLocalizedString(@"Cancel", nil) handler:nil];
        [alertView bk_addButtonWithTitle:NSLocalizedString(@"OK", nil) handler:^{
            
            UITextField *textField = [blockAlert textFieldAtIndex:0];
            [self.choices addObject:@{@"typeid": @1,@"pollfid":@"",@"value":textField.text}];
            [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[_choices count]-1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            
            
            
        }];
        [alertView show];
    }
}
@end
