//
//  DMPluginContactEditor.h
//  DMContentCreator
//
//  Created by Trash on 11/26/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMContentPlugins.h"

@interface DMPluginContactEditor : UITableViewController<DMContentPluginProtocol>

@end
