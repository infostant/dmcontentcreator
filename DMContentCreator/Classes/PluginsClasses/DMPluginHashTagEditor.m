//
//  DMPluginHashTagEditor.m
//  DMContentCreator
//
//  Created by Trash on 11/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#import "DMContentCreator.h"
#import "DMPluginHashTagEditor.h"
#import "DMContentCreatorStyle.h"
#import <LAUtilities/LAUtilities.h>
@interface DMPluginHashTagEditor (){
    BOOL isDismissing;
}
@property (weak, nonatomic) IBOutlet UITextField *hashtagTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *amountSegmentedControl;
@property (nonatomic,weak) DMContentPlugins *plugins;
@end

@implementation DMPluginHashTagEditor
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [DMContentCreatorStyle setNavigationBarStyle:self.navigationController];
    self.navigationItem.leftBarButtonItem = [DMContentCreatorStyle closeButtonWithHandler:^(UIBarButtonItem *weakSender) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    self.title = _plugins.pluginName;
    if ([UIDevice systemLessThan:@"7.0"]) {
        [self.tableView setBackgroundView:nil];
        [self.tableView setBackgroundColor:[[DMContentCreator sharedComponents] themeColor]];
    }
    
    [self.amountSegmentedControl setTintColor:[[DMContentCreator sharedComponents] color]];
    
    [_hashtagTextField setText:_plugins[DMCHashTagKey]];
    [_hashtagTextField setPlaceholder:NSLocalizedString(@"Hashtag", nil)];
    switch ([_plugins[DMCHashTagAmount] integerValue]) {
        case 18:
            [self.amountSegmentedControl setSelectedSegmentIndex:1];
            break;
        case 27:
            [self.amountSegmentedControl setSelectedSegmentIndex:2];
        default:
            [self.amountSegmentedControl setSelectedSegmentIndex:0];
            break;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    if ([self.hashtagTextField.text length]) {
        _plugins[DMCHashTagKey] = self.hashtagTextField.text;
    }else{
        [_plugins removeObjectForKey:DMCHashTagKey];
    }
    
    _plugins[DMCHashTagAmount] = @[@9,@18,@27][self.amountSegmentedControl.selectedSegmentIndex];
    
    NSArray *list = [_plugins checkIncompleteLists];
    NSLog(@"%@",list);
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.contentOffset.y < -170.0f && !isDismissing) {
        isDismissing = YES;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

//#pragma mark - Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @[@"Hashtag",@"Amount"][section];
    return NSLocalizedString(title, nil);
}

@end
