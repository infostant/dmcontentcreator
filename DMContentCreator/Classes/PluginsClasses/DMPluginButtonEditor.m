//
//  DMPluginButtonEditor.m
//  DMContentCreator
//
//  Created by Trash on 11/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//
#import "DMContentCreator.h"
#import "DMPluginButtonEditor.h"
#import "DMContentCreatorStyle.h"
#import <LAUtilities/LAUtilities.h>


@interface DMPluginButtonEditor (){
    BOOL isDismissing;
}
@property (strong, nonatomic) IBOutlet UITextField *buttonNameTextField;
@property (strong, nonatomic) IBOutlet UITextField *urlTextField;
@property (nonatomic,weak) DMContentPlugins *plugins;
@end

@implementation DMPluginButtonEditor


- (void)viewDidLoad
{
    [super viewDidLoad];

    [DMContentCreatorStyle setNavigationBarStyle:self.navigationController];
    self.navigationItem.leftBarButtonItem = [DMContentCreatorStyle closeButtonWithHandler:^(UIBarButtonItem *weakSender) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    self.title = _plugins.pluginName;
       if ([UIDevice systemLessThan:@"7.0"]) {
        [self.tableView setBackgroundView:nil];
        [self.tableView setBackgroundColor:[[DMContentCreator sharedComponents] themeColor]];
    }
    
    [_buttonNameTextField setText:_plugins[DMButtonName]];
    [_urlTextField setText:_plugins[DMCURLKey]];
    
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    if ([self.buttonNameTextField.text length]) {
        _plugins[DMButtonName] = self.buttonNameTextField.text;
    }else{
        [_plugins removeObjectForKey:DMButtonName];
    }
    
    if ([self.urlTextField.text length]) {
        _plugins[DMCURLKey] = self.urlTextField.text;
    }else{
        [_plugins removeObjectForKey:DMCURLKey];
    }
    
    NSArray *list = [_plugins checkIncompleteLists];
    NSLog(@"%@",list);
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.contentOffset.y < -170.0f && !isDismissing) {
        isDismissing = YES;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

//#pragma mark - Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @[@"Button name",@"URL"][section];
    return NSLocalizedString(title, nil);
}

@end