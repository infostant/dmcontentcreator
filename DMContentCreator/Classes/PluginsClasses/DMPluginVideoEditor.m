//
//  DMPluginVideoEditor.m
//  DMContentCreator
//
//  Created by Trash on 10/2/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMPluginVideoEditor.h"
#import "DMContentCreatorStyle.h"
#import <BlocksKit/BlocksKit.h>
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <HCYoutubeParser/HCYoutubeParser.h>
#import <LAUtilities/LAUtilities.h>
#import "DMContentCreator.h"
#import <BlocksKit/BlocksKit+UIKit.h>

@interface DMPluginVideoEditor ()<UIAlertViewDelegate>{
    BOOL isDismissing;
}
@property (weak, nonatomic) IBOutlet UIImageView *videoThubnailView;
@property (nonatomic,weak) DMContentPlugins *plugins;
@end

@implementation DMPluginVideoEditor


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = [DMContentCreatorStyle closeButtonWithHandler:^(UIBarButtonItem *weakSender) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    self.navigationItem.rightBarButtonItem = [DMContentCreatorStyle barButtonItemName:@"fontawesome##plus-sign" handler:^(UIBarButtonItem *weakSender) {
        [self openEditOption];
    }];
    [DMContentCreatorStyle setNavigationBarStyle:self.navigationController];
    self.title = _plugins.pluginName;
    
    if (_plugins[DMCCVideo]) {
        [_videoThubnailView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",_plugins[DMCCVideo]]]];
    }
    if ([UIDevice systemLessThan:@"7.0"]) {
        [self.tableView setBackgroundView:nil];
        [self.tableView setBackgroundColor:[[DMContentCreator sharedComponents] themeColor]];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_plugins checkIncompleteLists];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.contentOffset.y < -170.0f && !isDismissing) {
        isDismissing = YES;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - Table View delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self openEditOption];
}

#pragma mark - String Anlysis
-(void)openEditOption{
    UIActionSheet *actionSheet = [UIActionSheet bk_actionSheetWithTitle:NSLocalizedString(@"Youtube™ ID/URL", nil)];
    [actionSheet bk_addButtonWithTitle:NSLocalizedString(@"Paste from Clipboard", nil) handler:^{
        [self setFromUndefinedString:[[UIPasteboard generalPasteboard] string]];
    }];
    [actionSheet bk_addButtonWithTitle:NSLocalizedString(@"Edit", nil) handler:^{
        __block UIAlertView *blockAlert;
        UIAlertView *alertView = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Youtube™ ID/URL", nil)];
        [alertView setMessage:NSLocalizedString(@"Enter Youtube™ ID/URL below", nil)];
        blockAlert = alertView;
        [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alertView bk_setCancelButtonWithTitle:NSLocalizedString(@"Cancel", nil) handler:nil];
        [alertView bk_addButtonWithTitle:NSLocalizedString(@"OK", nil) handler:^{
            [self setFromUndefinedString:[[blockAlert textFieldAtIndex:0] text]];
        }];
        [alertView show];
        if (_plugins[DMCCVideo]) {
            [[alertView textFieldAtIndex:0] setText:_plugins[DMCCVideo]];
        }
    }];
    [actionSheet bk_setCancelButtonWithTitle:NSLocalizedString(@"Cancel", nil) handler:nil];
    [actionSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
}

-(void)setFromUndefinedString:(NSString *)string{
    NSURL *tmpURL = [NSURL URLWithString:[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    if ([[UIApplication sharedApplication] canOpenURL:tmpURL]) {
        NSString *ytid = [HCYoutubeParser youtubeIDFromYoutubeURL:tmpURL];
        if (ytid) {
            _plugins[DMCCVideo] = ytid;
            [_videoThubnailView setImage:nil];
            [_videoThubnailView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",_plugins[DMCCVideo]]]];
        }else{
            UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Warning", nil) message:NSLocalizedString(@"MSG for alert youtube invalid" , nil)];
            [alert bk_setCancelButtonWithTitle:NSLocalizedString(@"OK", nil) handler:nil];
            [alert show];
        }
    }else{
        _plugins[DMCCVideo] = string;
        [_videoThubnailView setImage:nil];
        [_videoThubnailView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",_plugins[DMCCVideo]]]];
    }
    if (_plugins[DMCCVideo]) {
        _plugins[DMCURLKey] = DMCCVideoURLFromYoutubeID(_plugins[DMCCVideo]);
    }else{
        [_plugins removeObjectForKey:DMCURLKey];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Paste link or Youtube™ URL", nil);
}

-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return NSLocalizedString(@"Paste link or Youtube™ URL", nil);
}

@end
