//
//  DMPluginHashTagEditor.h
//  DMContentCreator
//
//  Created by Trash on 11/21/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMContentPlugins.h"
@interface DMPluginHashTagEditor : UITableViewController<DMContentPluginProtocol>

@end
