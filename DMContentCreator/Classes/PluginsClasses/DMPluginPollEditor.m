//
//  DMPluginPollEditor.m
//  DMContentCreator
//
//  Created by Trash on 11/22/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMPluginPollEditor.h"
#import "DMContentCreator.h"
#import "DMContentCreatorStyle.h"
#import "DMPluginPollListEditor.h"
#import <LAUtilities/LAUtilities.h>
@interface DMPluginPollEditor (){
    BOOL isDismissing;
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *userType;
@property (weak, nonatomic) IBOutlet UITextField *pollTitleTextField;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (nonatomic,weak) DMContentPlugins *plugins;
@property (weak, nonatomic) IBOutlet UITableViewCell *otherCells;
@property (nonatomic,weak) NSMutableArray *choices;
@end

@implementation DMPluginPollEditor

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [DMContentCreatorStyle setNavigationBarStyle:self.navigationController];
    self.navigationItem.leftBarButtonItem = [DMContentCreatorStyle closeButtonWithHandler:^(UIBarButtonItem *weakSender) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    self.title = _plugins.pluginName;
    if ([UIDevice systemLessThan:@"7.0"]) {
        [self.tableView setBackgroundView:nil];
        [self.tableView setBackgroundColor:[[DMContentCreator sharedComponents] themeColor]];
    }
    
    [self.otherCells setTintColor:[[DMContentCreator sharedComponents] color]];
    [self.otherCells.textLabel setTextColor:[[DMContentCreator sharedComponents] color]];
    [self.countLabel setTextColor:[[DMContentCreator sharedComponents] color]];
    [self.pollTitleTextField setTextColor:[[DMContentCreator sharedComponents] color]];
    [self.userType setTintColor:[[DMContentCreator sharedComponents] color]];
    
    [self.userType setSelectedSegmentIndex:[_plugins[DMPollUserType] integerValue]];
    [self.pollTitleTextField setText:_plugins[DMPollTitle]];

    [self.otherCells setAccessoryType:[_plugins[DMPollOther] integerValue] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone];
    
    if (!_plugins[DMPollChoices]) {
        _plugins[DMPollChoices] = [NSMutableArray new];
    }
    
    self.choices = _plugins[DMPollChoices];
    
    NSUInteger index = 0;
    for (NSString *string in @[@"Guest",@"User",@"Pro"]) {
        [_userType setTitle:NSLocalizedString(string, nil) forSegmentAtIndex:index++];
    }
    [_otherCells.textLabel setText:NSLocalizedString(@"Other", nil)];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSString *choice = NSLocalizedString(@"Choices", nil);
    [choice stringByAppendingString:[NSString stringWithFormat:@" ( %lu )",(unsigned long)[_choices count]]];
    [self.countLabel setText:choice];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _plugins[DMPollUserType] = @(_userType.selectedSegmentIndex);
    _plugins[DMPollTitle] = _pollTitleTextField.text ? _pollTitleTextField.text : @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.contentOffset.y < -170.0f && !isDismissing) {
        isDismissing = YES;
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.tag == 11) {;
        _plugins[DMPollOther] = @(![_plugins[DMPollOther] integerValue]);
        [self.otherCells setAccessoryType:[_plugins[DMPollOther] integerValue] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    DMPluginPollListEditor *vc = segue.destinationViewController;
    [vc setChoices:self.choices];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @[@"User type",@"Poll title",@"Choices"][section];
    return NSLocalizedString(title,nil);
}

@end
