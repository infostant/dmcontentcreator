//
//  DMAddressSelector.m
//  DMContentCreator
//
//  Created by Trash on 11/25/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMAddressSelector.h"
#import "DMContentCreator.h"
@interface DMAddressSelector(){
    NSString *sqliteFile;
    sqlite3 *_database;
}
@end

@implementation DMAddressSelector

-(id)init{
    self = [super init];
    if (self) {
        sqliteFile = [[[DMContentCreator sharedComponents] resourceBundle] pathForResource:@"THAIADDR" ofType:@"sqlite"];
    }
    return self;
}

-(id)initWithContentOfFile:(NSString *)file{
    self = [self init];
    if (self) {
        sqliteFile = file;
    }
    return self;
}


-(NSMutableArray *)query:(NSString *)qureyString{
    NSMutableArray *rstArray = [NSMutableArray new];
    sqlite3_stmt    *statement;
    if (sqlite3_open([sqliteFile UTF8String], &_database) != SQLITE_OK) {
        NSLog(@"Failed to open database!");
    }else{
        const char *insert_stmt = [qureyString UTF8String];
        if (sqlite3_prepare_v2(_database, insert_stmt, -1, &statement, NULL) == SQLITE_OK) {
            int column = sqlite3_column_count(statement);
            
                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    NSMutableDictionary *sub = [NSMutableDictionary new];
                    for (int i = 0; i< column; i++) {
                        NSString *columnName = [NSString stringWithUTF8String:(char *)sqlite3_column_name(statement, i)];
                        
                        switch (sqlite3_column_type(statement, i)) {
                            case 1:
                                sub[columnName] = @(sqlite3_column_int(statement, i));
                                break;
                            case 4:
                                sub[columnName] = @(sqlite3_column_double(statement, i));
                                break;
                            default:
                                sub[columnName] = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, i)];
                                break;
                        }
                    }
                    [rstArray addObject:sub];
                }
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(_database);
    return rstArray;
}

-(NSMutableArray *)provinceList{
    return [self query:@"SELECT * FROM province ORDER BY name ASC"];
}

-(NSMutableArray *)districtListWithProID:(NSNumber *)proid{
    return [self query:[NSString stringWithFormat:@"SELECT * FROM district WHERE pro_id=%@ ORDER BY name ASC",proid]];
}

-(NSMutableArray *)subdistrictListWithDisID:(NSNumber *)disID proID:(NSNumber *)proid{
    return [self query:[NSString stringWithFormat:@"SELECT * FROM `sub-district` WHERE pro_id=%@ AND dist_id=%@ ORDER BY name ASC",proid,disID]];
}

-(NSString *)provinceWithProID:(NSNumber *)proid{
    if (!proid) {
        proid = @0;
    }
    NSString *queryString =[NSString stringWithFormat:@"SELECT name FROM province WHERE id=%@",proid];
    NSMutableArray *query = [self query:queryString];
    if ([query count]) {
        return query[0][@"name"];
    }
    return nil;
}

-(NSString *)districtWithDisID:(NSNumber *)distid{
    if (!distid) {
        distid = @0;
    }
    NSMutableArray *query = [self query:[NSString stringWithFormat:@"SELECT name FROM district WHERE id=%@",distid]];
    if ([query count]) {
        return query[0][@"name"];
    }
    return nil;
}

-(NSString *)subDistrictWithSubDistID:(NSNumber *)subdistID{
    if (!subdistID) {
        subdistID = @0;
    }
    NSMutableArray *query = [self query:[NSString stringWithFormat:@"SELECT name FROM `sub-district` WHERE id=%@",subdistID]];
    if ([query count]) {
        return query[0][@"name"];
    }
    return nil;
}

@end
