//
//  DMPickerViewController.m
//  DMContentCreator
//
//  Created by Supamard on 9/24/2557 BE.
//  Copyright (c) 2557 infostant. All rights reserved.
//

#import "DMPickerViewController.h"

@interface DMPickerViewController ()
{
    id<UIPickerViewDataSource> pickerDatasource;
    id<UIPickerViewDelegate>   pickerDelegate;
}

@end

@implementation DMPickerViewController

-(instancetype)initWithType:(DMPickerType)type
{
    if (self= [super init]) {
        if (type == DMPickerTypeDate) {
            self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 300, 180)];
            [self.datePicker setDatePickerMode:UIDatePickerModeTime];
            [self.datePicker setMinuteInterval:5];
            self.addressPicker = nil;
            [self.view addSubview:self.datePicker];
        }else if (type == DMPickerTypeAddress)
        {
            self.datePicker = nil;
            self.addressPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
            if (pickerDatasource && pickerDelegate) {
                self.addressPicker.delegate = pickerDelegate;
                self.addressPicker.dataSource = pickerDatasource;
            }
            
            [self.view addSubview:self.addressPicker];
        }
    }
    
    return self;
}

-(void)setAddressDelegate:(id<UIPickerViewDelegate>)delegate
        addressDatasource:(id<UIPickerViewDataSource>)datasource
{
    pickerDatasource = datasource;
    pickerDelegate   = delegate;
    if(self.addressPicker)
    {
        self.addressPicker.delegate = delegate;
        self.addressPicker.dataSource = datasource;
    }
}

@end
