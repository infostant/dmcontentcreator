//
//  DMContentCreator.m
//  DMContentCreator
//
//  Created by Trash on 9/17/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMContentCreator.h"
#import <iOS7Colors/UIColor+iOS7Colors.h>
#import <WTGlyphFontSet/WTGlyphFontSet.h>
#import <BlocksKit/BlocksKit.h>
#import <RNGridMenu/RNGridMenu.h>
#import "DMContentPlugins.h"
#import "DMContentCreatorCell.h"
#import <LAUtilities/LAUtilities.h>
#import "DMPluginContentInformationEditor.h"
#import "DMContentCreatorStyle.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#define SYSTEM_AVALIABLE_PLUGIN @[@0,@1,@3,@4,@5,@6,@7,@8,@10,@13,@14,@18,@21]
#define DMCTEMPFILE @"__AUTOSAVE__"
#define DMCDATSFILE @"datasource.ds"
#import <BlocksKit/UIAlertView+BlocksKit.h>

@interface DMContentCreator ()<RNGridMenuDelegate>{
    NSString *resourcesBundle;
    Class navigationClass;
    NSMutableArray *dataSource;
    UIStoryboard *mainStoryboard;
    UIStatusBarStyle backupStatusBarStyle;
    NSOperationQueue *queue;
    BOOL isUploading;
}
@end

@implementation DMContentCreator

#pragma mark - Plugins protocol

-(void)setDefaultPlugins:(NSArray *)defaultPlugins{
    NSMutableArray *muArr = [NSMutableArray new];
    [muArr addObject:@0];
    for (NSNumber *num in defaultPlugins) {
        if ([self isPluginAvaliable:num]) {
            [muArr addObject:num];
        }
    }
    _defaultPlugins = [NSArray arrayWithArray:muArr];
}

#pragma mark - ViewController Event
- (void)viewDidLoad
{
    [super viewDidLoad];

    queue = [[NSOperationQueue alloc] init];
    queue.maxConcurrentOperationCount = 1;
     backupStatusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
    resourcesBundle = @"DMContentCreator.bundle";
    [[DMContentCreator sharedComponents] setResourceBundle:[NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"DMContentCreator" ofType:@"bundle"]]];
    if (self.navigationController) {
        navigationClass = [self.navigationController class];
    }
    [[DMContentCreator sharedComponents] setColor:_color];
    [[DMContentCreator sharedComponents] setThemeColor:(_themeMode == DMContentCreatorBackgroundModeDark ? [UIColor blackColor] : [UIColor whiteColor])];
    [[DMContentCreator sharedComponents] setButtonColor:_buttonColor];
    [[DMContentCreator sharedComponents] setInvertedNavigation:_invertedNavigation];
    [[DMContentCreator sharedComponents] setThemeMode:_themeMode];
    [DMContentCreatorStyle setNavigationBarStyle:self.navigationController];
    [[DMContentCreator sharedComponents] setTagsList:self.tagsList];
    [[DMContentCreator sharedComponents] setUserData:self.userData];
    [self.tableView setBackgroundColor:[[DMContentCreator sharedComponents] themeColor]];
    [self prepareDirectory];
    if ([[self.file lastPathComponent] isEqualToString:DMCTEMPFILE]) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:[self.file  stringByAppendingPathComponent:DMCDATSFILE]]) {
            NSLog(@"ds : %@",[self.file stringByAppendingPathComponent:DMCDATSFILE]);
            dataSource = [NSKeyedUnarchiver unarchiveObjectWithFile:[self.file stringByAppendingPathComponent:DMCDATSFILE]];
        }else{
            dataSource = [NSMutableArray new];
            for (id plugid in _defaultPlugins) {
                [dataSource addObject:[DMContentPlugins pluginWithIdentifier:[plugid unsignedIntegerValue]]];
            }
            for (id plugid in _sampleLayoutPlugins) {
                BOOL found = NO;
                for (NSNumber *av in SYSTEM_AVALIABLE_PLUGIN) {
                    if ([av isEqualToNumber:plugid]) {
                        found = YES;
                        break;
                    }
                }
                if (found) {
                    [dataSource addObject:[DMContentPlugins pluginWithIdentifier:[plugid unsignedIntegerValue]]];
                }
            }

        }
    }else{
        dataSource = [NSKeyedUnarchiver unarchiveObjectWithFile:[self.file stringByAppendingPathComponent:DMCDATSFILE]];
    }
    
    if (!self.navigationController || [[self.navigationController viewControllers] count] == 1) {
        UIBarButtonItem *closeButton = [DMContentCreatorStyle closeButtonWithHandler:^(UIBarButtonItem *weakSender) {
            UIAlertView *alertSave = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Quit", nil) message:NSLocalizedString(@"MSG for alert save", nil)];
            [alertSave bk_addButtonWithTitle:NSLocalizedString(@"Save", nil) handler:^{
                [self save:^{
                    [self dismissWithAnimated:YES];
                }];
            }];
            [alertSave bk_setCancelButtonWithTitle:NSLocalizedString(@"Don't Save", nil) handler:^{
                [self dismissWithAnimated:YES];
                if ([[self.file lastPathComponent] isEqualToString:DMCTEMPFILE]) {
                    [[NSFileManager defaultManager] removeItemAtPath:self.file error:nil];
                }
            }];
            [alertSave bk_setCancelButtonWithTitle:NSLocalizedString(@"Cancel", nil) handler:nil];
            [alertSave show];
        }];
        self.navigationItem.leftBarButtonItem = closeButton;
        self.navigationItem.title   = NSLocalizedString(@"New Content", nil);
    }
    
    UIBarButtonItem *taskButton = [DMContentCreatorStyle barButtonItemName:@"fontawesome##tasks" handler:^(UIBarButtonItem *sender){
        if (sender.enabled) {
            [self showMenu];
        }
    }];
    self.navigationItem.rightBarButtonItem = taskButton;
    self.navigationController.navigationBar.translucent = NO;
    
}

-(void)setSampleLayoutPlugins:(NSArray *)sampleLayoutPlugins{
    NSMutableArray *arr = [NSMutableArray new];
    for (id plugid in sampleLayoutPlugins) {
        BOOL found = NO;
        for (NSNumber *av in SYSTEM_AVALIABLE_PLUGIN) {
            if ([av isEqualToNumber:plugid]) {
                found = YES;
                break;
            }
        }
        if (found) {
            [arr addObject:plugid];
        }
    }
    _sampleLayoutPlugins = arr;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self updateStatusBar];
    [DMContentCreatorStyle setNavigationBarStyle:self.navigationController];
    [self.tableView reloadData];
    [NSKeyedArchiver archiveRootObject:dataSource toFile:[self.file stringByAppendingPathComponent:DMCDATSFILE]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self autoSave];
}


-(NSMutableDictionary *)parameterFromPluginDatasource:(NSArray *)__ds oauth:(NSString *)oauth{
    NSMutableDictionary *product = [NSMutableDictionary new];
    NSMutableArray *ds = [NSMutableArray arrayWithArray:__ds];
    if (oauth) {
        product[@"oauth"] = oauth;
    }
    product[@"fid"] = _featureIdentifier;
    
    DMContentPlugins *infoPlugin = [__ds objectAtIndex:0];
    if ([infoPlugin.pluginIdentifier isEqualToNumber:@0]) {
        [product addEntriesFromDictionary:[infoPlugin generatedDataWithPath:_file]];
        [ds removeObjectAtIndex:0];
        product[@"plugin"] = [NSMutableDictionary new];
        NSUInteger number = 0;
        NSUInteger i;
        for (i=0; i<[ds count]; i++) {
            DMContentPlugins *eachPlugin = [ds objectAtIndex:i];
            NSMutableDictionary *generatedData = [eachPlugin generatedDataWithPath:_file];
            if ([generatedData count] > 1) {
                product[@"plugin"][@(number++)] = generatedData;
            }
        }
        if (product[@"showComment"]) {
            NSLog(@"product[showcomment] = %@",product[@"showComment"]);
            if ([product[@"showComment"] isEqualToNumber:@YES]) {
                product[@"plugin"][@(i)] = @{@"plugid": @17,@"status":@1};
            }
            [product removeObjectForKey:@"showComment"];
        }
        return product;
    }
    return nil;
}
#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DMContentPlugins *plugin = dataSource[indexPath.row];
    if ([plugin isKindOfClass:[DMContentPlugins class]]) {
        [plugin checkIncompleteLists];
    }
    UIImage *image,*line;
    if ([[UIDevice currentDevice].model isEqualToString:@"iPhone"] || [[UIDevice currentDevice].model isEqualToString:@"iPhone Simulator"]) {
        image = [UIImage imageNamed:[resourcesBundle stringByAppendingPathComponent:@"item-background-gray"]];
        line =[UIImage imageNamed:[resourcesBundle stringByAppendingPathComponent:@"line"]];
    }else{
        image = [UIImage imageNamed:[resourcesBundle stringByAppendingPathComponent:@"item-background-gray_ipad"]];
        line =[UIImage imageNamed:[resourcesBundle stringByAppendingPathComponent:@"line_ipad"]];
    }
    DMContentCreatorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if ([[dataSource objectAtIndex:indexPath.row] isKindOfClass:[NSString class]] &&
        
        [[dataSource objectAtIndex:indexPath.row] isEqualToString:@"DUMMY"]) {
        [cell.pluginTitleLabel setText:nil];
        [cell.pluginDetailsLabel setTextColor:[UIColor iOS7lightGrayColor]];
        [cell.pluginDetailsLabel setText:nil];
        [cell.thumbnailView setImage:nil];
        [cell setBackgroundView:nil];
        cell.hidden = YES;
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:123];
        [imageView setImage:nil];
        cell.backgroundImage.image = nil;
        [imageView removeFromSuperview];
    }
    else {
        cell.hidden= NO;
        NSString *string = [NSString stringWithFormat:@"DMCCDESCRIPT%02lu",(unsigned long)[plugin.pluginIdentifier unsignedIntegerValue]];
        [cell.pluginDetailsLabel setText:NSLocalizedString(string, @"detail description")];
        cell.backgroundImage.image = (indexPath.row >= [_defaultPlugins count]) ? image : line;
        [cell.pluginTitleLabel setTextColor:_color];
        [cell.pluginTitleLabel setText:( plugin.pluginName )];
        [cell.pluginDetailsLabel setTextColor:[UIColor iOS7lightGrayColor]];
               [cell.thumbnailView setImage:plugin.thumbnail];
        UILabel *pluginTitleLabel = cell.pluginTitleLabel;
        UILabel *pluginDetailsLabel     = cell.pluginDetailsLabel;
        NSDictionary *dict = NSDictionaryOfVariableBindings(pluginTitleLabel);
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[pluginTitleLabel]|" options:0 metrics:nil views:dict]];
        dict = NSDictionaryOfVariableBindings(pluginDetailsLabel);
        [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[pluginDetailsLabel]|" options:0 metrics:nil views:dict]];
    }
    [cell setBackgroundColor:[[DMContentCreator sharedComponents] themeColor]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row >= [[self defaultPlugins ] count];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [dataSource removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DMContentPlugins *plugin = dataSource[indexPath.row];
    NSString *storyboardIdentifier =[NSString stringWithFormat:@"DMEPLG%02u",plugin.pluginIdentifier.unsignedIntegerValue];
    if ([self storyboardName:@"iPhone-DMContentCreator" hasStoryboardIdentifier:storyboardIdentifier]) {
        UIViewController<DMContentPluginProtocol> *editView = [self.storyboard instantiateViewControllerWithIdentifier:storyboardIdentifier];
        [editView setPlugins:plugin];
        if ([editView respondsToSelector:@selector(setSavePath:)]) {
            [editView setSavePath:self.file];
        }
        UINavigationController *nav = [[[navigationClass class] alloc] initWithRootViewController:editView];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return  (indexPath.row >= [_defaultPlugins count]) ;
}

-(NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath{
    return (proposedDestinationIndexPath.row < [_defaultPlugins count]) ?sourceIndexPath :proposedDestinationIndexPath;
}

#pragma mark - BVReorderTableView Delegate

-(id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath{
    id object = [dataSource objectAtIndex:indexPath.row];
    [dataSource replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
    return object;
}

-(void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath{
    DMContentPlugins *plugin = [dataSource objectAtIndex:fromIndexPath.row];
    [dataSource removeObjectAtIndex:fromIndexPath.row];
    [dataSource insertObject:plugin atIndex:toIndexPath.row];
}

#pragma mark - General Method

-(void)save:(void(^)(void))saved{
    if (self.isSaved) {
        BOOL success = [NSKeyedArchiver archiveRootObject:dataSource toFile:[self.file stringByAppendingPathComponent:DMCDATSFILE]];
        NSLog(@"success = %u",success);
        if (saved) {
            saved();
        }
    }else{
        DMContentPlugins *infoPlugin =[dataSource objectAtIndex:0];
        if ([infoPlugin[DMCCProductNameKey] length]) {
            NSString *productName = infoPlugin[DMCCProductNameKey];
            if ([[NSFileManager defaultManager] fileExistsAtPath:[[self saveDiretory] stringByAppendingPathComponent:productName]]) {
                NSUInteger duplicateRunNumber = 1;
                while ([[NSFileManager defaultManager] fileExistsAtPath:[[self saveDiretory] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@ %lu",productName,(unsigned long)++duplicateRunNumber]]]);
                productName =[NSString stringWithFormat:@"%@ %lu",productName,(unsigned long)duplicateRunNumber];
            }
            NSLog(@"product name : %@",productName);
            NSString *toPath =[[self saveDiretory] stringByAppendingPathComponent:productName];
            [[NSFileManager defaultManager] createDirectoryAtPath:toPath withIntermediateDirectories:YES attributes:nil error:nil];
            NSString *fromPath = self.file;
            NSArray *fileList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fromPath error:nil];
            for (NSString *eachFileName in fileList) {
                [[NSFileManager defaultManager] moveItemAtPath:[fromPath stringByAppendingPathComponent:eachFileName] toPath:[toPath stringByAppendingPathComponent:eachFileName] error:nil];
            }
            [[NSFileManager defaultManager] removeItemAtPath:fromPath error:nil];
            _file = toPath;
            NSLog(@"datasource = %@",dataSource);
            [NSKeyedArchiver archiveRootObject:dataSource toFile:[self.file stringByAppendingPathComponent:DMCDATSFILE]];
            if (saved) {
                saved();
            }
        }else{
            UIAlertView *alertProductName = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Product name required", nil) message:NSLocalizedString(@"MSG for alert product name required", nil)];
            [alertProductName bk_addButtonWithTitle:@"OK" handler:^{
                [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            }];
            [alertProductName show];
        }
    }
    
}

-(void)autoSave{
    [NSKeyedArchiver archiveRootObject:dataSource toFile:[self.file stringByAppendingPathComponent:DMCDATSFILE]];
}

-(BOOL)isSaved{
    return  ![[[self.file pathComponents] lastObject] isEqualToString:DMCTEMPFILE];
}

-(void)showMenu{
    RNGridMenuItem *addItem = [[RNGridMenuItem alloc]
                               initWithImage:[UIImage imageGlyphNamed:@"fontawesome##plus-sign" height:100 color: _color]
                               title:NSLocalizedString(@"Add", nil) action:^{
        [self showAddMenu];
    }];
    
    RNGridMenuItem *upload = [[RNGridMenuItem alloc] initWithImage:[UIImage
                                                                    imageGlyphNamed:@"fontawesome##upload"
                                                                    height:100
                                                                    color: _color]
                                                             title:NSLocalizedString(@"Upload", nil) action:^{
        [self uploadToServer];
    }];
    RNGridMenuItem *save = [[RNGridMenuItem alloc] initWithImage:[UIImage
                                                                  imageGlyphNamed:@"fontawesome##save"
                                                                  height:100 color: _color]
                                                           title:NSLocalizedString(@"Saved", nil)
                                                          action:^{
        [self save:^{
            UIAlertView *alertView = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Saved", nil) message:NSLocalizedString(@"MSG for alert saved", nil)];
            [alertView bk_addButtonWithTitle:NSLocalizedString(@"OK", nil) handler:nil];
            [alertView show];
        }];
    }];
    NSArray *mar = @[addItem,save,upload];
    
    RNGridMenu *gridMenu = [[RNGridMenu alloc] initWithItems:mar];
    [gridMenu setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8f]];
    [gridMenu setItemTextColor:[UIColor iOS7darkGrayColor]];
    [gridMenu setHighlightColor:[_color colorWithAlphaComponent:0.1]];
    [gridMenu showInViewController:self center:CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f)];
}

-(void)showAddMenu{
    NSMutableArray *mar = [NSMutableArray new];
    for (id __plugid in _avaliablePlugins) {
        if ([self isPluginAvaliable:__plugid]) {
            NSUInteger plugid = [__plugid unsignedIntegerValue];
            NSString *localizedPluginName = [NSString stringWithFormat:@"DMCONTENTPLUGIN-%lu",(unsigned long)plugid];
            
            RNGridMenuItem *tmpItem = [[RNGridMenuItem alloc] initWithImage:[UIImage imageGlyphNamed:[DMContentPlugins imageNameForPluginIdentifier:plugid] height:100 color: _color] title:NSLocalizedString(localizedPluginName, @"Use your local name by localizable.string") action:^{
                [dataSource addObject:[DMContentPlugins pluginWithIdentifier:plugid]];
                [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:[dataSource count]-1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            }];
            [mar addObject:tmpItem];
        }
    }
    RNGridMenu *gridMenu = [[RNGridMenu alloc] initWithItems:mar];
    [gridMenu setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8f]];
    [gridMenu setItemTextColor:[UIColor iOS7darkGrayColor]];
    [gridMenu setHighlightColor:[_color colorWithAlphaComponent:0.1]];
    [gridMenu showInViewController:self center:CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f)];
    
}

-(BOOL)isPluginAvaliable:(NSNumber *)__plugid{
    for (NSNumber *num in SYSTEM_AVALIABLE_PLUGIN) {
        if ([__plugid isEqualToNumber:num]) {
            return YES;
        }
    }
    return NO;
}

+(instancetype)contentCreatorForIPhoneDevice{
    return [[UIStoryboard storyboardWithName:@"iPhone-DMContentCreator" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"CTCAT"];
}

+(DMContentCreatorComponents *)sharedComponents{
    static dispatch_once_t onceToken;
    static DMContentCreatorComponents *sharedComponents = nil;
    dispatch_once(&onceToken, ^{
        sharedComponents = [DMContentCreatorComponents new];
        sharedComponents.color = [UIColor iOS7lightBlueColor];
        sharedComponents.navigationClass = [UINavigationController class];
    });
    return sharedComponents;
}

-(void)updateStatusBar{
    BOOL isDarkMode = ([[DMContentCreator sharedComponents] themeMode] == DMContentCreatorBackgroundModeDark);
    [[UIApplication sharedApplication] setStatusBarStyle:(([[DMContentCreator sharedComponents] invertedNavigation] && isDarkMode)||(!([[DMContentCreator sharedComponents] invertedNavigation] || isDarkMode)) ? UIStatusBarStyleLightContent : UIStatusBarStyleDefault) animated:YES];
}

+(UIImage *)backImage{
    return [UIImage
            imageGlyphNamed:@"fontawesome#angle-left"
            size:CGSizeMake(25, 25)
            color:([[DMContentCreator sharedComponents] invertedNavigation] ? [[DMContentCreator sharedComponents] color] : [[DMContentCreator sharedComponents] themeColor])];
}

-(BOOL)storyboardName:(NSString *)storyboardName hasStoryboardIdentifier:(NSString *)storyboardIdentifier{
    NSArray *list = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[NSBundle mainBundle] pathForResource:storyboardName ofType:@"storyboardc"] error:nil];
    for (NSString *file in list) {
        NSString *fileName = [file componentsSeparatedByString:@"."][0];
        if ([fileName isEqualToString:storyboardIdentifier]) {
            return YES;
        }
    }
    return NO;
}

-(void)finishReorderingWithObject:(id)object atIndexPath:(NSIndexPath *)indexPath{
    [dataSource replaceObjectAtIndex:indexPath.row withObject:object];
}

+(NSString *)generateImageFileFromPath:(NSString *)path extension:(NSString *)extension{
    NSString *imageName = nil;
    do {
        imageName = [NSString stringWithFormat:@"%u.%@",arc4random(),extension];
    } while ([[NSFileManager defaultManager] fileExistsAtPath:[path stringByAppendingPathComponent:imageName]]);
    return imageName;
}

-(void)prepareDirectory{
    NSString *librayPath = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0];
    NSString *mainDirName = @"DMCCSAVE";
    NSString *fullMainDirName =[librayPath stringByAppendingPathComponent:mainDirName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:fullMainDirName]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:fullMainDirName withIntermediateDirectories:YES attributes:@{NSFileProtectionKey: NSFileProtectionComplete} error:nil];
    }
    
    NSString *featureDirName = [NSString stringWithFormat:@"F%02lu",(unsigned long)[self.featureIdentifier unsignedIntegerValue]];
    NSString *fullFeatureDirName = [fullMainDirName stringByAppendingPathComponent:featureDirName];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:fullFeatureDirName]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:fullFeatureDirName withIntermediateDirectories:YES attributes:@{NSFileProtectionKey: NSFileProtectionComplete} error:nil];
    }
    
    if (!_file) {
        _file = [fullFeatureDirName stringByAppendingPathComponent:DMCTEMPFILE];
    }
    [[NSFileManager defaultManager] createDirectoryAtPath:self.file withIntermediateDirectories:YES attributes:@{NSFileProtectionKey: NSFileProtectionComplete} error:nil];
}

-(NSString *)saveDiretory{
    return [DMContentCreator saveDiretoryForFeatureIdentifier:[self.featureIdentifier unsignedIntegerValue]];
}

+(NSString *)saveDiretoryForFeatureIdentifier:(NSUInteger )fid{
    return [[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"DMCCSAVE"] stringByAppendingPathComponent:[NSString stringWithFormat:@"F%02lu",(unsigned long)fid]];
}

-(void)uploadToServer{
    //required list
    if ((!_featureIdentifier || !_baseURL || !_userData[@"oauth"])) {
        return;
    }
    
    UIBarButtonItem *closeButton = self.navigationItem.leftBarButtonItem;
    self.navigationItem.rightBarButtonItem.enabled = NO;

    NSArray *reqList = nil;
    for (int i = 0; i < [_defaultPlugins count]; i++) {
        DMContentPlugins *eachPlugin = dataSource[i];
        reqList = [eachPlugin checkIncompleteLists];
        if ([reqList count]) {
            break;
        }else{
            reqList = nil;
        }
    }
    if (reqList) {
        UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Some data required" , nil) message:NSLocalizedString(@"MSG for alert product data required", [reqList componentsJoinedByString:@", "])];
        [alert bk_setCancelButtonWithTitle:NSLocalizedString(@"OK", nil) handler:nil];
        [alert show];
    }else{
        isUploading = YES;
        MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo:self.view.superview animated:YES];
        progress.mode = MBProgressHUDModeIndeterminate;
        progress.progress = 0.0;
        progress.labelText = NSLocalizedString(@"Preparing", nil);
        __block NSMutableDictionary *param;
        NSBlockOperation *dataGenerator = [NSBlockOperation blockOperationWithBlock:^{
            param = [self parameterFromPluginDatasource:dataSource oauth:_userData[@"oauth"]];
        }];
        [dataGenerator setCompletionBlock:^{
            progress.labelText = NSLocalizedString(@"Uploading", nil);
            if (param) {
                AFHTTPClient *client = [[AFHTTPClient alloc] initWithBaseURL:self.baseURL];
                [client postPath:@"ajax/saveproductmobiledata" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    self.navigationItem.leftBarButtonItem = closeButton;
                    self.navigationItem.rightBarButtonItem.enabled = YES;
                    [progress hide:YES];
                    if (!responseObject[@"error"][@"error"]) {
                        if ([self.file isEqualToString:DMCTEMPFILE] ) {
                            UIAlertView *alertSuccess = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Success", nil) message:NSLocalizedString(@"MSG for alert upload completely", nil)];
                            [alertSuccess bk_setCancelButtonWithTitle:NSLocalizedString(@"Close", nil) handler:^{
                                [self dismissWithAnimated:YES];
                            }];
                            [alertSuccess show];
                        }else{
                            if ([[[self.file pathComponents] lastObject] isEqualToString:DMCTEMPFILE]) {
                                UIAlertView *alertSuccess = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Success", nil) message:NSLocalizedString(@"MSG for alert upload completely", nil)];
                                [alertSuccess bk_setCancelButtonWithTitle:NSLocalizedString(@"Close", nil) handler:^{
                                    [[NSFileManager defaultManager] removeItemAtPath:self.file error:nil];
                                    [self dismissWithAnimated:YES];
                                }];
                                [alertSuccess show];
                            }else{
                                UIAlertView *alertSuccess = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Success", nil) message:NSLocalizedString(@"MSG for alert upload completely & save", nil)];
                                [alertSuccess bk_addButtonWithTitle:NSLocalizedString(@"Delete", nil) handler:^{
                                    [[NSFileManager defaultManager] removeItemAtPath:self.file error:nil];
                                    [self dismissWithAnimated:YES];
                                }];
                                [alertSuccess bk_setCancelButtonWithTitle:NSLocalizedString(@"Close", nil) handler:^{
                                    [self dismissWithAnimated:YES];
                                }];
                                [alertSuccess show];
                            }
                        }
                    }else{
                        NSString *errMessage = responseObject[@"error"][@"error"][0][@"msg"];
                        UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Error", nil) message:errMessage];
                        [alert bk_addButtonWithTitle:NSLocalizedString(@"OK", nil) handler:^{
                            self.navigationItem.leftBarButtonItem = closeButton;
                            self.navigationItem.rightBarButtonItem.enabled = YES;
                        }];
                        [alert show];
                    }

                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [progress hide:YES];
                    UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Error", nil) message:error.localizedDescription];
                    [alert bk_addButtonWithTitle:NSLocalizedString(@"OK", nil) handler:^{
                        self.navigationItem.leftBarButtonItem = closeButton;
                        self.navigationItem.rightBarButtonItem.enabled = YES;
                    }];
                    [alert show];

                }];
                
//                [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
//                    float percentDone = ((float)((int)totalBytesWritten) / (float)((int)totalBytesExpectedToWrite));
//                    progress.progress = percentDone;
//                    NSLog(@"progress %f",percentDone);
//                }];
//                [queue addOperation:operation];
            }
        }];
        [queue addOperation:dataGenerator];
        UIBarButtonItem *stopButton = [DMContentCreatorStyle barButtonItemName:@"fontawesome##remove-sign" handler:^(UIBarButtonItem *weakSender) {
            UIAlertView *alert = [UIAlertView bk_alertViewWithTitle:NSLocalizedString(@"Stop", nil) message:NSLocalizedString(@"MSG for alert stop upload"  , nil)];
            [alert bk_addButtonWithTitle:NSLocalizedString(@"Stop", nil) handler:^{
                [queue cancelAllOperations];
                [progress hide:YES];
                self.navigationItem.leftBarButtonItem = closeButton;
                self.navigationItem.rightBarButtonItem.enabled = YES;
            }];
            [alert bk_addButtonWithTitle:NSLocalizedString(@"Continue", nil) handler:nil];
            [alert show];
        }];
        self.navigationItem.leftBarButtonItem = stopButton;
    }
   
}

-(void)dismissWithAnimated:(BOOL)animated{
    [self dismissViewControllerAnimated:animated completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:backupStatusBarStyle animated:NO];
    }];
}

+(NSArray *)fileListForFeautreIdentifier:(NSUInteger)featureIdentifier{
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"DMCCSAVE"] stringByAppendingPathComponent:[NSString stringWithFormat:@"F%02u",featureIdentifier]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        return nil;
    }
    return [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:nil];
}

+(BOOL)deleteSaveName:(NSString *)fileToDelete featureIdentifier:(NSUInteger)featureIdentifier{
   NSString *path = [[[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"DMCCSAVE"] stringByAppendingPathComponent:[NSString stringWithFormat:@"F%02u",featureIdentifier]] stringByAppendingPathComponent:fileToDelete];
    return [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}
@end


@implementation DMContentCreatorComponents
-(UIColor *)foregroundColor{
    return ([self invertedNavigation] ? [self color] : [self themeColor]);
}
@end