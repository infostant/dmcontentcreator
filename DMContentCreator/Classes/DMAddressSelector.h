//
//  DMAddressSelector.h
//  DMContentCreator
//
//  Created by Trash on 11/25/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface DMAddressSelector : NSObject

-(NSMutableArray *)provinceList;
-(NSString *)provinceWithProID:(NSNumber *)proid;
-(NSMutableArray *)districtListWithProID:(NSNumber *)proid;
-(NSString *)districtWithDisID:(NSNumber *)distid;
-(NSMutableArray *)subdistrictListWithDisID:(NSNumber *)disID proID:(NSNumber *)proid;
-(NSString *)subDistrictWithSubDistID:(NSNumber *)subdistID;
-(id)initWithContentOfFile:(NSString *)file;
@end
