//
//  DMPickerViewController.h
//  DMContentCreator
//
//  Created by Supamard on 9/24/2557 BE.
//  Copyright (c) 2557 infostant. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    DMPickerTypeDate = 0,
    DMPickerTypeAddress = 1
}DMPickerType;

@interface DMPickerViewController : UIViewController

@property (strong ,nonatomic) UIDatePicker *datePicker;
@property (strong ,nonatomic) UIPickerView *addressPicker;

-(instancetype)initWithType:(DMPickerType)type;
-(void)setAddressDelegate:(id<UIPickerViewDelegate>)delegate
        addressDatasource:(id<UIPickerViewDataSource>)datasource;
@end
