//
//  DMMapView.m
//  DMContentCreator
//
//  Created by Trash on 11/1/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMMapView.h"
#import <LAUtilities/LAUtilities.h>

@implementation DMMapView

-(void)layoutSublayersOfLayer:(CALayer *)layer{
    if ([UIDevice systemGreaterThanOrEqualTo:@"7.0"]) {
        [super layoutSublayersOfLayer:layer];
    }
}

@end
