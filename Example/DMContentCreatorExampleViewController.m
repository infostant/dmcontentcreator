//
//  DMContentCreatorExampleViewController.m
//  DMContentCreator
//
//  Created by Trash on 9/27/13.
//  Copyright (c) 2013 infostant. All rights reserved.
//

#import "DMContentCreatorExampleViewController.h"
#import "DMContentCreator.h"
#import <iOS7Colors/UIColor+iOS7Colors.h>

#import "DMAddressSelector.h"
@interface DMContentCreatorExampleViewController (){
    DMAddressSelector *add;
    NSArray *systemTags;
    CGFloat hue,bright,sat,alpha;
}
@property (weak, nonatomic) IBOutlet UIButton *btnLight;
@property (weak, nonatomic) IBOutlet UIButton *btnInvLight;
@property (weak, nonatomic) IBOutlet UIButton *btnDark;
@property (weak, nonatomic) IBOutlet UIButton *btnInvDark;
@property (weak, nonatomic) IBOutlet UISlider *slider;

@end

@implementation DMContentCreatorExampleViewController
@synthesize color;
-(id)init{
    self = [super initWithNibName:@"DMContentCreatorExampleViewController" bundle:nil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    
//    NSArray *stringsArray = [NSArray arrayWithObjects:
//                             @"เอ",
//                             @"อำ",
//                             @"สำ",
//                             @"เก",
//                             @"กา", nil];
//    static NSStringCompareOptions comparisonOptions = NSCaseInsensitiveSearch | NSNumericSearch |
//    NSWidthInsensitiveSearch | NSForcedOrderingSearch;
//    NSLocale *currentLocale = [NSLocale currentLocale];
//    NSComparator finderSort = ^(id string1, id string2) {
//        NSRange string1Range = NSMakeRange(0, [string1 length]);
//        return [string1 compare:string2 options:comparisonOptions range:string1Range locale:currentLocale];
//    };
//    
//    NSArray* sortedArray = [stringsArray sortedArrayUsingComparator:finderSort];
//    for (id obj in sortedArray) {
//        NSLog(@"%@",obj);
//    }
//    NSLog(@"finderSort: %@", sortedArray);
    
    [super viewDidLoad];
//    color =[UIColor iOS7pinkColor];
    systemTags = @[@{@"tagid": @100,@"name":@"Apple"},
                   @{@"tagid": @101,@"name":@"Banana"},
                   @{@"tagid": @102,@"name":@"Blackcurrant"},
                   @{@"tagid": @103,@"name":@"Blueberry"},
                   @{@"tagid": @104,@"name":@"Coconut"},
                   @{@"tagid": @105,@"name":@"Cherry"},
                   @{@"tagid": @106,@"name":@"Grape"},
                   @{@"tagid": @107,@"name":@"Dragonfruit"},
                   @{@"tagid": @108,@"name":@"Grape"},
                   @{@"tagid": @109,@"name":@"Jackfruit"},
                   @{@"tagid": @110,@"name":@"Apricot"},
                   @{@"tagid": @111,@"name":@"Avocado"},
                   @{@"tagid": @112,@"name":@"Blackberry"},
                   @{@"tagid": @113,@"name":@"Guava"},
                   @{@"tagid": @114,@"name":@"Kiwi fruit"},
                   @{@"tagid": @115,@"name":@"Lemon"},
                   @{@"tagid": @116,@"name":@"Lime"},
                   @{@"tagid": @117,@"name":@"Loquat"},
                   @{@"tagid": @118,@"name":@"Lychee"},
                   @{@"tagid": @119,@"name":@"Cantaloupe"},
                   @{@"tagid": @121,@"name":@"Watermelon"},
                   @{@"tagid": @122,@"name":@"Orange"},
                   @{@"tagid": @123,@"name":@"Papaya"},
                   @{@"tagid": @124,@"name":@"Passionfruit"},
                   @{@"tagid": @125,@"name":@"Peach"},
                   @{@"tagid": @126,@"name":@"Pear"},
                   @{@"tagid": @127,@"name":@"Pineapple"},
                   @{@"tagid": @128,@"name":@"Pomelo"},
                   @{@"tagid": @129,@"name":@"Purple Mangosteen"},
                   @{@"tagid": @130,@"name":@"Raspberry"},
                   @{@"tagid": @131,@"name":@"Rambutan"},
                   @{@"tagid": @132,@"name":@"Redcurrant"},
                   @{@"tagid": @133,@"name":@"Star fruit"},
                   @{@"tagid": @134,@"name":@"Strawberry"},
                   @{@"tagid": @135,@"name":@"Tomato"},
                   ];
    [_btnLight setBackgroundColor:color];
    [_btnInvLight setTitleColor:color forState:UIControlStateNormal];
    [_btnDark setBackgroundColor:color];
    [_btnInvDark setTitleColor:color forState:UIControlStateNormal];
    self.title = @"Select Theme";
    [self.view setBackgroundColor:color];
    [color getHue:&hue saturation:&sat brightness:&bright alpha:&alpha];
    _slider.value = hue;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)light:(id)sender {
    DMContentCreator *viewController = [DMContentCreator contentCreatorForIPhoneDevice];
    [self setVC:viewController];
    [viewController setThemeMode:DMContentCreatorBackgroundModeLight];
    [viewController setInvertedNavigation:NO];
    [viewController setTagsList:systemTags];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navigation animated:YES completion:nil];
}
- (IBAction)addProduct:(id)sender {
    DMContentCreator *viewController = [DMContentCreator contentCreatorForIPhoneDevice];
    [self setVC:viewController];
    [viewController setThemeMode:DMContentCreatorBackgroundModeLight];
    [viewController setInvertedNavigation:YES];
    [viewController setTagsList:systemTags];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navigation animated:YES completion:nil];
}
- (IBAction)dark:(id)sender {
    DMContentCreator *viewController = [DMContentCreator contentCreatorForIPhoneDevice];
    [self setVC:viewController];
    [viewController setThemeMode:DMContentCreatorBackgroundModeDark];
    [viewController setInvertedNavigation:NO];
    [viewController setTagsList:systemTags];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navigation animated:YES completion:nil];
}
- (IBAction)invDark:(id)sender {
    DMContentCreator *viewController = [DMContentCreator contentCreatorForIPhoneDevice];
    [self setVC:viewController];
    [viewController setThemeMode:DMContentCreatorBackgroundModeDark];
    [viewController setInvertedNavigation:YES];
    [viewController setTagsList:systemTags];
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:viewController];
    [self presentViewController:navigation animated:YES completion:nil];
}
- (IBAction)progressDidChange:(UISlider *)sender {
    UIColor *___color = [UIColor colorWithHue:sender.value saturation:sat brightness:bright alpha:alpha];
    color = ___color;
    self.view.backgroundColor = color;
    [_btnLight setBackgroundColor:color];
    [_btnInvLight setTitleColor:color forState:UIControlStateNormal];
    [_btnDark setBackgroundColor:color];
    [_btnInvDark setTitleColor:color forState:UIControlStateNormal];
}

-(void)setVC:(DMContentCreator *)vc{
    [vc setFeatureIdentifier:@1];
    [vc setColor:color];
    [vc setButtonColor:color];
    [vc setBaseURL:[NSURL URLWithString:@"http://v19.dmconnex.com"]];
//    [vc setOauth:@""];
    [vc setUserData:@{@"oauth": @"62a7793a85c3ef2e8b96b493e58d6fd3",@"facebook":@"http://www.faceboook.com/iwutz",@"proid":@144,@"disid":@14408,@"userid":@10}];
    
//    [vc setDefaultPlugins: @[@6,@7]];
    [vc setDefaultPlugins:@[]];
//    [vc setSampleLayoutPlugins:@[@14,@17]];
    [vc setSampleLayoutPlugins:@[@1,
                                 @5,
                                 @13,
                                 @14,
                                 @17,
                                 @18,
                                 @21,
                                 @22,
                                 @23
                                 ]];
//    [vc setAvaliablePlugins:@[@3,@14,@5,@8,@17,@10]];
    [vc setAvaliablePlugins:@[@1,
                              @3,
                              @5,
                              @8,
                              @10,
                              @13,
                              @14,
                              @17,
                              @21,
                              @22,
                              @18,
                              @23]];
//    [vc setFile:@"รีีัันรีัััรรัพุรนนดด"];
//    [vc setDefaultPlugins: @[@6,@7]];
//    [vc setSampleLayoutPlugins:@[@4,@8,@3,@10]];
//    [vc setAvaliablePlugins:@[@4,@8,@3,@10,@14,@5]];
}

@end
